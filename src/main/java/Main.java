import java.sql.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void print(String s) {
        Logger log = Logger.getLogger("Logger");
        log.setLevel(Level.ALL);
        log.log(Level.INFO, s);
    }

    private static final String ENTERTABLE = "Enter table name: \n";
    private static final String TABLENAME = "$tableName";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String url = "jdbc:mysql://localhost:3306/schoolmain?serverTimezone=Europe/Moscow&useSSL=false";
        String username = "root";
        String pass = "root";
        String name = "";
        String columnname = "";
        String deleteword = "";
        String sql;

        int rows;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            print("Connection to School DB succesfull!");
            print("Choose you want to do:");
            print("1) Show table\n" +
                    "2) Delete record from table\n" +
                    "3) Add record to table\n" +
                    "4) Edit the record in the table\n");
            print("Enter your choice:");
            int num = in.nextInt();
            in.nextLine();
            try (Connection conn = DriverManager.getConnection(url, username, pass)){
                Statement statement = conn.createStatement();
                PreparedStatement preparedStatement;
                ResultSet resultSet;
                ResultSetMetaData rsmd;
                int columnsNumber;
                try {
                    switch (num) {
                        case 1:
                            print(ENTERTABLE);
                            name = in.nextLine();
                            sql = "SELECT * FROM $tableName";
                            sql = sql.replace(TABLENAME, name);
                            preparedStatement = conn.prepareStatement(sql);
                            try {
                                resultSet = preparedStatement.executeQuery();
                                rsmd = resultSet.getMetaData();
                                columnsNumber = rsmd.getColumnCount();
                                while (resultSet.next()) {
                                    for (int i = 1; i <= columnsNumber; i++) {
                                        String columnValue = resultSet.getString(i);
                                        print(rsmd.getColumnName(i) + ": " + columnValue);
                                    }
                                }
                                resultSet.close();
                            } finally {
                                preparedStatement.close();
                            }
                            break;
                        case 2:
                            print(ENTERTABLE);
                            name = in.nextLine();
                            print("Enter the name of " + name + " table column: \n");
                            columnname = in.nextLine();
                            print("Enter the delete word: \n");
                            deleteword = in.nextLine();
                            sql = "DELETE FROM $tableName where $columnName = '$deleteWord'";
                            sql = sql.replace(TABLENAME, name);
                            sql = sql.replace("$columnName", columnname);
                            sql = sql.replace("$deleteWord", deleteword);
                            rows = statement.executeUpdate(sql);
                            print("%d row(s) deleted " + rows);
                            break;
                        case 3:
                            print(ENTERTABLE);
                            name = in.nextLine();
                            sql = "SELECT * FROM $tableName";
                            sql = sql.replace(TABLENAME, name);
                            preparedStatement = conn.prepareStatement(sql);
                            try {
                                resultSet = preparedStatement.executeQuery();
                                rsmd = resultSet.getMetaData();
                                columnsNumber = rsmd.getColumnCount();
                                String[] columnName = new String[columnsNumber];
                                StringBuilder columns = new StringBuilder();
                                StringBuilder data = new StringBuilder();
                                int k = 0;
                                for (int i = 1; i <= columnsNumber; i++) {
                                    columnName[i-1] = rsmd.getColumnLabel(i);
                                    if (columnName[i-1].contains("id")) {
                                        continue;
                                    } else {
                                        k++;
                                    }

                                    if (k > 1) {
                                        columns.append(", ").append(columnName[i - 1]);
                                    } else {
                                        columns.append(columnName[i - 1]);
                                    }
                                    print("Enter data in " + columnName[i-1] + " column: ");
                                    String datas = in.nextLine();
                                    if (k > 1) {
                                        data.append(", '").append(datas).append("'");
                                    } else {
                                        data.append("'").append(datas).append("'");
                                    }
                                }
                                sql = "INSERT INTO $tableName($columns) VALUES ($data)";
                                sql = sql.replace(TABLENAME, name);
                                sql = sql.replace("$columns", columns.toString());
                                sql = sql.replace("$data", data.toString());
                                rows = statement.executeUpdate(sql);
                                print("\nAdded %d rows" + rows);
                            } finally {
                                preparedStatement.close();
                            }
                            break;
                        case 4:
                            print(ENTERTABLE);
                            name = in.nextLine();
                            print("Enter the name of " + name + " table column: \n");
                            columnname = in.nextLine();
                            print("Enter the update word: \n");
                            String updateword = in.nextLine();
                            print("Enter the name of column in condition: \n");
                            String condition = in.nextLine();
                            print("Enter the condition word: \n");
                            String conditionword = in.nextLine();
                            sql = "UPDATE $tableName SET $columnName = '$updateWord' WHERE $condition = '$cWord'";
                            sql = sql.replace(TABLENAME, name);
                            sql = sql.replace("$columnName", columnname);
                            sql = sql.replace("$updateWord", updateword);
                            sql = sql.replace("$condition", condition);
                            sql = sql.replace("$cWord", conditionword);
                            rows = statement.executeUpdate(sql);
                            print("Updated %d rows" + rows);
                            break;
                        default:
                            print("You must enter a value in the range from 1 to 4");
                            break;
                    }
                } finally {
                    statement.close();
                }
                in.close();
            }
        }
        catch(Exception ex){
            print("Connection failed...");
        }
    }
}
